/**
 * Methods for the collection interface
 * @author Jackson Kwan
 */
package jacksonkwantest2q3;
import java.util.*;

public class CollectionMethods {
	/**
	 * Returns collection of planets with radius bigger or equal to size
	 * @param planets planet collection
	 * @param size size to be compared to
	 * @return returned arraylist, which is a collection
	 */
	public static Collection<Planet> getLargerThan(Collection<Planet>planets, double size){
		Collection<Planet> returned = new ArrayList<Planet>();
		for (int i=0; i<planets.size(); i++) {
			if (planets.get(i).getRadius >= size) {
				returned.add(planets.get(i));
			}
		}
		return returned;
	}
}
