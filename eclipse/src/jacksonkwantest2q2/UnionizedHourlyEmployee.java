/**
 * Wage calculation with overtime taken into account
 * @author Jackson Kwan
 */
package jacksonkwantest2q2;

public class UnionizedHourlyEmployee extends HourlyEmployee{
	private int maxHoursPerWeek;
	private double overtimeRate;
	
	/**
	 * Constructor with parameters, including super
	 * @param weeklyHours hours worked in a week
	 * @param hourlyWage hourly pay
	 * @param maxHoursPerWeek maximum hours to determine overtime
	 * @param overtimeRate payment increase rate if overtime
	 */
	public UnionizedHourlyEmployee(int weeklyHours, double hourlyWage, int maxHoursPerWeek, double overtimeRate) {
		super(weeklyHours,hourlyWage);
		this.maxHoursPerWeek = maxHoursPerWeek;
		this.overtimeRate = overtimeRate;
	}
	
	/**
	 * gets weekly pay and checks if theres overtime or not
	 */
	public double getWeeklyPay() {
		if (super.getWeeklyHours()<= maxHoursPerWeek) {
			return super.getWeeklyHours()*super.getHourlyWage();
		}
		else {
			double holder = 0;
			holder = maxHoursPerWeek*super.getHourlyWage();
			holder = holder + super.getHourlyWage()*overtimeRate*(super.getWeeklyHours()-maxHoursPerWeek);
			return holder;
		}
	}
}
