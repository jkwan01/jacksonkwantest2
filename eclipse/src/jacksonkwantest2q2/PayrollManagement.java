/**
 * Main application to calculate payment of employees
 * @author Jackson Kwan
 */
package jacksonkwantest2q2;

public class PayrollManagement {
	/**
	 * Main method
	 */
	public static void main(String[] args) {
		Employee[] testArr = new Employee[5];
		testArr[0] = new SalariedEmployee(50000);
		testArr[1] = new SalariedEmployee(35000);
		testArr[2] = new HourlyEmployee(34, 14);
		testArr[3] = new UnionizedHourlyEmployee(46, 20, 30, 1.4);
		testArr[4] = new UnionizedHourlyEmployee(30, 15, 40, 1.5);
		
		System.out.println(getTotalExpenses(testArr));
	}
	
	/**
	 * calculates the total pay of employees
	 * @param input employee array with their payment information
	 * @return the total amount owed
	 */
	public static double getTotalExpenses(Employee[] input) {
		double total = 0;
		for (int i=0; i<input.length;i++) {
			total = total + input[i].getWeeklyPay();
		}
		return total;
	}
}
