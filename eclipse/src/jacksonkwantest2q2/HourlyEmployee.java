/**
 * Wage calculation for a regular employee
 * @author Jackson Kwan
 */
package jacksonkwantest2q2;

public class HourlyEmployee implements Employee{
	private int weeklyHours;
	private double hourlyWage;
	
	/**
	 * constructor with parameters
	 * @param weeklyHours hours worked in a week
	 * @param hourlyWage pay per hour
	 */
	public HourlyEmployee(int weeklyHours, double hourlyWage) {
		this.weeklyHours = weeklyHours;
		this.hourlyWage = hourlyWage;
	}
	
	/**
	 * weekly pay calculation
	 */
	public double getWeeklyPay() {
		return weeklyHours*hourlyWage;
	}

	/**
	 * getters for the 2 parameters
	 */
	public int getWeeklyHours() {
		return weeklyHours;
	}

	public double getHourlyWage() {
		return hourlyWage;
	}
	
	
}
